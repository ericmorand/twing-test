const http = require('http');
const url = require('url');
const port = 3000;

const Twing = require('twing');

let loader = new Twing.TwingLoaderFilesystem('.');

const TwingExtension = require('twing/lib/extension').default;
const TwingFunction = require('twing/lib/function').default;

class SleepExtension extends TwingExtension {
    getFunctions() {
        return [
            new TwingFunction('sleep', function(duration) {
                return new Promise((resolve) => setTimeout(resolve, duration));
            })
        ];
    }
}

let twing = new Twing.TwingEnvironment(loader);

twing.addExtension(new SleepExtension());

async function requestHandler(request, response) {
    response.setHeader('Content-Type', 'text/html; charset=UTF-8');
    response.setHeader('Transfer-Encoding', 'chunked');

    let query = url.parse(request.url, true).query;

    process.stdout.write = function(chunk) {
        response.write(chunk);
    };

    await twing.render('tick.html.twig', query);

    console.log('ARE');

    response.end();
}

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
});