const Twing = require('twing');

let loader = new Twing.TwingLoaderFilesystem('.');
let twing = new Twing.TwingEnvironment(loader);
let data = {
    name: 'Fabien',
    body: 'POTENCIER'
};

// write the rendered template to process.stdout
twing.display('index.html.twig', data);

// write the rendered template to the output buffer - i.e. should not output anything
twing.render('index.html.twig', data);

// write the template source code to the console
// note that console.log write to process.stdout
console.warn(twing.compile(twing.parse(twing.tokenize(twing.getLoader().getSourceContext('index.html.twig')))));