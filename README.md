## Usage

`npm start`

The console will show both the rendered template and the template source code.

`npm start > ~/Documents/index.html`

~/Documents/index.html will contain the rendered template while the console will show the template source code.

`npm run server`

Open http://localhost:3000. Your browser will receive a partial HTTP response once every second (or so). Pass a `name` parameter to the URL to customize the output - for example `http://localhost:3000?name=World!`. The magic here is that it is *not* the server that is polling the response but the twig template himself.

## How it works

Have a look at `index.js` and `server.js` for details.